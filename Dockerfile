FROM python:3.8-slim-buster
MAINTAINER ivan

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt

RUN apt update && apt install gcc postgresql-server-dev-all netcat -y

RUN pip install -r /requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./app /app
COPY ./wait.sh /wait.sh

RUN adduser --no-create-home app
RUN chown app /wait.sh
RUN chmod +x /wait.sh
USER app
